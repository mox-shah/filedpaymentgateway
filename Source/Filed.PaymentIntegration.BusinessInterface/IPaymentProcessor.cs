﻿namespace Filed.PaymentIntegration.BusinessInterface
{
    using Filed.PaymentIntegration.BusinessModel;

    /// <summary>
    /// Signature of Payment Processor
    /// </summary>
    public interface IPaymentProcessor
    {
        /// <summary>
        /// Processes the payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        PaymentResponse ProcessPayment(PaymentRequest paymentRequest);
    }
}
