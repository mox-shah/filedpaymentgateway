﻿namespace Filed.PaymentIntegration.BusinessManager.Common
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// Retry Logic implementation
    /// </summary>
    public static class RetryLogic
    {
        /// <summary>
        /// Does the specified action.
        /// </summary>
        /// <param name="action">The action.</param>
        /// <param name="retryInterval">The retry interval.</param>
        /// <param name="maxAttemptCount">The maximum attempt count.</param>
        public static void Do(
            Action action,
            TimeSpan retryInterval,
            int maxAttemptCount = 3)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, retryInterval, maxAttemptCount);
        }

        /// <summary>
        /// Does the specified action.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action">The action.</param>
        /// <param name="retryInterval">The retry interval.</param>
        /// <param name="maxAttemptCount">The maximum attempt count.</param>
        /// <returns></returns>
        public static RetryResult Do<T>(
            Func<T> action,
            TimeSpan retryInterval,
            int maxAttemptCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int attempted = 0; attempted < maxAttemptCount; attempted++)
            {
                try
                {
                    if (attempted > 0)
                    {
                        Thread.Sleep(retryInterval);
                    }
                    var actionResponse = action();
                    var retryResult = new RetryResult(exceptions);
                    retryResult.actionResponse = actionResponse;
                    retryResult.isSuccess = true; 
                    return retryResult;
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                }
            }

            return new RetryResult(exceptions);
        }
    }
    /// <summary>
    /// Retry Result response
    /// </summary>
    public class RetryResult
    {
        /// <summary>
        /// The failure exceptions
        /// </summary>
        public readonly IEnumerable<Exception> failureExceptions;
        /// <summary>
        /// The failure count
        /// </summary>
        public readonly int failureCount;
        /// <summary>
        /// The action response
        /// </summary>
        public object actionResponse;
        /// <summary>
        /// Gets or sets a value indicating whether this instance is success.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is success; otherwise, <c>false</c>.
        /// </value>
        public bool isSuccess { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RetryResult"/> class.
        /// </summary>
        /// <param name="failureExceptions">The failure exceptions.</param>
        protected internal RetryResult(
            ICollection<Exception> failureExceptions)
        {
            this.failureExceptions = failureExceptions;
            failureCount = failureExceptions.Count;
            isSuccess = false;
        }
    }
}
