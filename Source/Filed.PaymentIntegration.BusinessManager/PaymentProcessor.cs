﻿namespace Filed.PaymentIntegration.BusinessManager
{
    using System;
    using Filed.PaymentIntegration.BusinessInterface;
    using Filed.PaymentIntegration.BusinessManager.Common;
    using Filed.PaymentIntegration.BusinessModel;
    using Filed.PaymentIntegration.DataAccess;
    using Filed.PaymentIntegration.PaymentGateway.Interface;

    /// <summary>
    /// Business implementation for Payment Process
    /// </summary>
    /// <seealso cref="Filed.PaymentIntegration.BusinessInterface.IPaymentProcessor" />
    public class PaymentProcessor : IPaymentProcessor
    {
        /// <summary>
        /// The service provider
        /// </summary>
        IServiceProvider serviceProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentProcessor" /> class.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        public PaymentProcessor(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Processes the payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        public PaymentResponse ProcessPayment(PaymentRequest paymentRequest)
        {
            ValidatePaymentRequest(paymentRequest);
            if (paymentRequest.Amount < BusinessConstants.CHEAPPAYMENTLIMIT)
            {
                var cheapPaymentProcessor = (ICheapPaymentGateway)serviceProvider.GetService(typeof(ICheapPaymentGateway));
                return cheapPaymentProcessor.ProcessCheapPayment(paymentRequest);
            }
            else if (paymentRequest.Amount <= BusinessConstants.EXPENSIVEPAYMENTLIMIT)
            {
                var expensivePaymentProcessor = (IExpensivePaymentGateway)serviceProvider.GetService(typeof(IExpensivePaymentGateway));
                return expensivePaymentProcessor.ProcessExpensivePayment(paymentRequest);
            }
            else
            {
                var expensivePaymentProcessor = (IPremiumPaymentGateway)serviceProvider.GetService(typeof(IPremiumPaymentGateway));
                var retryResult = RetryLogic.Do(() => expensivePaymentProcessor.ProcessPremiumPayment(paymentRequest), TimeSpan.FromSeconds(1), 3);
                if (retryResult.isSuccess)
                {
                    return (PaymentResponse)retryResult.actionResponse;
                }
                else
                {
                    return new PaymentResponse
                    {
                        TransactionId = Guid.NewGuid(),
                        RetryCount = retryResult.failureCount,
                        PaymentDetail = paymentRequest,
                        IsSuccess = false
                    };
                }
            }
        }

        /// <summary>
        /// Validates the payment request.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <exception cref="System.InvalidOperationException">
        /// User details doesn't match!
        /// or
        /// Your card is expired!
        /// </exception>
        private void ValidatePaymentRequest(PaymentRequest paymentRequest)
        {
            var isValidUser = ValidateUser(paymentRequest);
            if (!isValidUser)
            {
                throw new InvalidOperationException("User details doesn't match!");
            }
            if (paymentRequest.ExpirationDate < DateTime.Now)
            {
                throw new InvalidOperationException("Your card is expired!");
            }
        }
        /// <summary>
        /// Validates the user.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        private bool ValidateUser(PaymentRequest paymentRequest)
        {
            var userDataAccess = new UserInformation();
            return userDataAccess.VerifyUserDetail(new DataAccess.DataModels.UserDetails()
            {
                UserName = paymentRequest.CardHolder,
                CardNo = paymentRequest.CreditCardNumber.Replace("-", ""),
                CVV = paymentRequest.SecurityCode,
                ExpirationDate = paymentRequest.ExpirationDate
            });
        }
    }
}
