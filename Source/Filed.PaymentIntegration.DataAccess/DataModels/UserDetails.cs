﻿namespace Filed.PaymentIntegration.DataAccess.DataModels
{
    using System;

    /// <summary>
    /// User Detail data model
    /// </summary>
    public class UserDetails
    {
        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the card no.
        /// </summary>
        /// <value>
        /// The card no.
        /// </value>
        public string CardNo { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the CVV.
        /// </summary>
        /// <value>
        /// The CVV.
        /// </value>
        public int CVV { get; set; }
    }
}
