﻿namespace Filed.PaymentIntegration.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Filed.PaymentIntegration.DataAccess.DataModels;

    /// <summary>
    /// User Information
    /// </summary>
    public class UserInformation
    {
        /// <summary>
        /// The user details
        /// </summary>
        public List<UserDetails> userDetails;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserInformation"/> class.
        /// </summary>
        public UserInformation()
        {
            userDetails = new List<UserDetails>() {
                new UserDetails(){
                    UserName = "Mox Shah",
                    CardNo = "4375510789011233",
                    CVV = 111,
                    ExpirationDate = new DateTime(2021,11,01)
                },
                new UserDetails(){
                    UserName = "Andrew Filed",
                    CardNo = "4375510789011100",
                    CVV = 222,
                    ExpirationDate = new DateTime(2022,11,01)
                },
                new UserDetails(){
                    UserName = "Andrei Luchici",
                    CardNo = "4375510789010011",
                    CVV = 333,
                    ExpirationDate = new DateTime(2020,11,01)
                },
            };
        }

        /// <summary>
        /// Verifies the user detail.
        /// </summary>
        /// <param name="userDetail">The user detail.</param>
        /// <returns></returns>
        public bool VerifyUserDetail(UserDetails userDetail)
        {
            return this.userDetails.Any(u => u.UserName == userDetail.UserName && u.CardNo == userDetail.CardNo && u.CVV == userDetail.CVV && u.ExpirationDate == userDetail.ExpirationDate);
        }
    }
}
