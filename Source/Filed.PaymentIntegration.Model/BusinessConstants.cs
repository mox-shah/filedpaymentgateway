﻿namespace Filed.PaymentIntegration.BusinessManager.Common
{
    /// <summary>
    /// Business constatns
    /// </summary>
    public static class BusinessConstants
    {
        /// <summary>
        /// The cheappaymentlimit
        /// </summary>
        public static decimal CHEAPPAYMENTLIMIT = 21;
        /// <summary>
        /// The expensivepaymentlimit
        /// </summary>
        public static decimal EXPENSIVEPAYMENTLIMIT = 500;
        /// <summary>
        /// The premiumpaymentlimit
        /// </summary>
        public static decimal PREMIUMPAYMENTLIMIT = 501;
        /// <summary>
        /// The premiumretrycount
        /// </summary>
        public static decimal PREMIUMRETRYCOUNT = 3;

    }

    /// <summary>
    /// Payment Mode
    /// </summary>
    public enum PaymentMode
    {
        /// <summary>
        /// The cheap
        /// </summary>
        Cheap = 1,
        /// <summary>
        /// The expensive
        /// </summary>
        Expensive = 2,
        /// <summary>
        /// The premium
        /// </summary>
        Premium = 3
    }
}
