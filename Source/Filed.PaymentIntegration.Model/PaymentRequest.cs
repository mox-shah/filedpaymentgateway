﻿namespace Filed.PaymentIntegration.BusinessModel
{
    using System;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Payment request
    /// </summary>
    public class PaymentRequest
    {
        /// <summary>
        /// Gets or sets the credit card number.
        /// </summary>
        /// <value>
        /// The credit card number.
        /// </value>
        [Required(ErrorMessage = "Please provide Credit Card No."), CreditCard(ErrorMessage = "Please provide valid Credit Card No.")]
        public string CreditCardNumber { get; set; }

        /// <summary>
        /// Gets or sets the card holder.
        /// </summary>
        /// <value>
        /// The card holder.
        /// </value>
        [Required(ErrorMessage = "Please provide Card Holder Name.")]
        public string CardHolder { get; set; }

        /// <summary>
        /// Gets or sets the expiration date.
        /// </summary>
        /// <value>
        /// The expiration date.
        /// </value>
        [PresentOrFutureDateAttribute]
        public DateTime ExpirationDate { get; set; }

        /// <summary>
        /// Gets or sets the security code.
        /// </summary>
        /// <value>
        /// The security code.
        /// </value>
        public int SecurityCode { get; set; }

        /// <summary>
        /// Gets or sets the amount.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        [Required(ErrorMessage = "Please enter amount.")]
        public decimal Amount { get; set; }
    }
}
