﻿namespace Filed.PaymentIntegration.PaymentGateway.Implementation
{
    using System;
    using Filed.PaymentIntegration.BusinessManager.Common;
    using Filed.PaymentIntegration.BusinessModel;
    using Filed.PaymentIntegration.PaymentGateway.Interface;

    /// <summary>
    /// Implementation of Cheap Payment Gateway
    /// </summary>
    /// <seealso cref="ICheapPaymentGateway" />
    public class CheapPaymentGateway : ICheapPaymentGateway
    {
        /// <summary>
        /// Processes the cheap payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        public PaymentResponse ProcessCheapPayment(PaymentRequest paymentRequest)
        {
            return new PaymentResponse()
            {
                TransactionId = Guid.NewGuid(),
                RetryCount = 1,
                IsSuccess = true,
                PaymentType = PaymentMode.Cheap,
                PaymentDetail = paymentRequest
            };
        }
    }
}
