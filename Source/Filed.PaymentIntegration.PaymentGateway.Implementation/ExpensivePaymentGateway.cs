﻿namespace Filed.PaymentIntegration.PaymentGateway.Implementation
{
    using System;
    using Filed.PaymentIntegration.BusinessManager.Common;
    using Filed.PaymentIntegration.BusinessModel;
    using Filed.PaymentIntegration.PaymentGateway.Interface;

    /// <summary>
    /// Implementation of Expensive Payment Gateway
    /// </summary>
    /// <seealso cref="IExpensivePaymentGateway" />
    public class ExpensivePaymentGateway : IExpensivePaymentGateway
    {
        /// <summary>
        /// Processes the expensive payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        public PaymentResponse ProcessExpensivePayment(PaymentRequest paymentRequest)
        {
            return new PaymentResponse()
            {
                TransactionId = Guid.NewGuid(),
                RetryCount = 1,
                IsSuccess = true,
                PaymentType = PaymentMode.Expensive,
                PaymentDetail = paymentRequest
            };
        }
    }
}
