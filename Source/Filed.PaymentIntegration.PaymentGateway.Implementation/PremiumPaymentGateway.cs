﻿namespace Filed.PaymentIntegration.PaymentGateway.Implementation
{
    using System;
    using System.Threading;
    using Filed.PaymentIntegration.BusinessManager.Common;
    using Filed.PaymentIntegration.BusinessModel;
    using Filed.PaymentIntegration.PaymentGateway.Interface;

    /// <summary>
    /// Implementation of Premium Payment Gateway
    /// </summary>
    /// <seealso cref="IPremiumPaymentGateway" />
    public class PremiumPaymentGateway : IPremiumPaymentGateway
    {

        /// <summary>
        /// Processes the premium payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        public PaymentResponse ProcessPremiumPayment(PaymentRequest paymentRequest)
        {
            ////Putting failure to show capability of retry logic
            if (paymentRequest.Amount == 999)
            {
                Thread.Sleep(2000);
                throw new Exception("Known exception to show capability of retry logic");
            }
            return new PaymentResponse()
            {
                TransactionId = Guid.NewGuid(),
                RetryCount = 1,
                IsSuccess = true,
                PaymentType = PaymentMode.Premium,
                PaymentDetail = paymentRequest
            };
        }
    }
}
