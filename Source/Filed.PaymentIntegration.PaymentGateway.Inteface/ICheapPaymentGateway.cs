﻿namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    using Filed.PaymentIntegration.BusinessModel;

    /// <summary>
    /// Signatures of Cheap Payment Gateway
    /// </summary>
    public interface ICheapPaymentGateway
    {
        /// <summary>
        /// Processes the cheap payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        PaymentResponse ProcessCheapPayment(PaymentRequest paymentRequest);
    }
}
