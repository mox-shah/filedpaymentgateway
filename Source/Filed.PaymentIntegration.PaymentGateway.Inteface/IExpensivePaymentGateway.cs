﻿namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    using Filed.PaymentIntegration.BusinessModel;

    /// <summary>
    /// Signatures of Expensive Payment Gateway
    /// </summary>
    public interface IExpensivePaymentGateway
    {
        /// <summary>
        /// Processes the expensive payment.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <returns></returns>
        PaymentResponse ProcessExpensivePayment(PaymentRequest paymentRequest);
    }
}
