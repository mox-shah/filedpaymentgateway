﻿namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    using Filed.PaymentIntegration.BusinessModel;

    /// <summary>
    /// Signatures of Premium Payment Gateway
    /// </summary>
    public interface IPremiumPaymentGateway
    {
        PaymentResponse ProcessPremiumPayment(PaymentRequest paymentRequest);
    }
}
