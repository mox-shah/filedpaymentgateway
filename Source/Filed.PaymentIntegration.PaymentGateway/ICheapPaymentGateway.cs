﻿using Filed.PaymentIntegration.BusinessModel;

namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    public interface ICheapPaymentGateway
    {
        PaymentResponse ProcessCheapPayment(PaymentRequest paymentRequest);
    }
}
