﻿namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    using Filed.PaymentIntegration.BusinessModel;

    public interface IExpensivePaymentGateway
    {
        PaymentResponse ProcessExpensivePayment(PaymentRequest paymentRequest);
    }
}
