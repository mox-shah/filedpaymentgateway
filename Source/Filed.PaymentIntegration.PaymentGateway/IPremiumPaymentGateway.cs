﻿using Filed.PaymentIntegration.BusinessModel;

namespace Filed.PaymentIntegration.PaymentGateway.Interface
{
    public interface IPremiumPaymentGateway
    {
        PaymentResponse ProcessPremiumPayment(PaymentRequest paymentRequest);
    }
}
