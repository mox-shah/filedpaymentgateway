namespace Filed.PaymentIntegration.Test
{
    using System;
    using Filed.PaymentIntegration.BusinessInterface;
    using Filed.PaymentIntegration.BusinessManager;
    using Filed.PaymentIntegration.BusinessManager.Common;
    using Filed.PaymentIntegration.BusinessModel;
    using Filed.PaymentIntegration.PaymentGateway.Implementation;
    using Filed.PaymentIntegration.PaymentGateway.Interface;
    using Filed.PaymentIntegration.Web.Controllers;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.VisualStudio.TestTools.UnitTesting;


    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class PaymentControllerTest
    {
        IServiceProvider serviceProvider;
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            var services = new ServiceCollection();
            services.AddTransient<ICheapPaymentGateway, CheapPaymentGateway>();
            services.AddTransient<IExpensivePaymentGateway, ExpensivePaymentGateway>();
            services.AddTransient<IPremiumPaymentGateway, PremiumPaymentGateway>();
            services.AddTransient<IPaymentProcessor, PaymentProcessor>();
            serviceProvider = services.BuildServiceProvider();
        }

        /// <summary>
        /// Tests the method1.
        /// </summary>
        [TestMethod]
        public void TestPaymentProcess()
        {
            var paymentProcessor = (IPaymentProcessor)serviceProvider.GetService(typeof(IPaymentProcessor));
            PaymentController controller = new PaymentController(serviceProvider, paymentProcessor);
            var paymentRequest = GetPaymentRequest();
            ValidateUserDetails(controller);
            ValidateCheapPaymentMethod(paymentRequest, controller);
            ValidatePremiumPaymentMethod(paymentRequest, controller);
            ValidateExpensivePaymentMethod(paymentRequest, controller);
            ValidateRetryCountForPremium(controller);
        }

        /// <summary>
        /// Validates the cheap payment method.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        private void ValidateCheapPaymentMethod(PaymentRequest paymentRequest, PaymentController controller)
        {
            paymentRequest.Amount = BusinessConstants.CHEAPPAYMENTLIMIT;
            var data = controller.ProcessPayment(paymentRequest);
            Assert.AreEqual(PaymentMode.Cheap, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Premium, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Expensive, data.PaymentType);
        }

        /// <summary>
        /// Validates the premium payment method.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <param name="controller">The controller.</param>
        private void ValidatePremiumPaymentMethod(PaymentRequest paymentRequest, PaymentController controller)
        {
            paymentRequest.Amount = BusinessConstants.PREMIUMPAYMENTLIMIT;
            var data = controller.ProcessPayment(paymentRequest);
            Assert.AreEqual(PaymentMode.Premium, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Cheap, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Expensive, data.PaymentType);
        }

        /// <summary>
        /// Validates the premium payment method.
        /// </summary>
        /// <param name="paymentRequest">The payment request.</param>
        /// <param name="controller">The controller.</param>
        private void ValidateExpensivePaymentMethod(PaymentRequest paymentRequest, PaymentController controller)
        {
            paymentRequest.Amount = BusinessConstants.EXPENSIVEPAYMENTLIMIT;
            var data = controller.ProcessPayment(paymentRequest);
            Assert.AreEqual(PaymentMode.Expensive, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Cheap, data.PaymentType);
            Assert.AreNotEqual(PaymentMode.Premium, data.PaymentType);
        }

        [ExpectedException(typeof(InvalidOperationException))]
        private void ValidateUserDetails(PaymentController controller)
        {
            var paymentRequest = GetPaymentRequest();
            paymentRequest.SecurityCode = new Random().Next(1000, 9999);
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                controller.ProcessPayment(paymentRequest);
            });
        }

        private void ValidateRetryCountForPremium(PaymentController controller)
        {
            var paymentRequest = GetPaymentRequest();
            paymentRequest.Amount = 999;
            var response = controller.ProcessPayment(paymentRequest);
            Assert.AreEqual(3, response.RetryCount);
            Assert.AreEqual(false, response.IsSuccess);
        }

        /// <summary>
        /// Gets the payment request.
        /// </summary>
        /// <returns></returns>
        private PaymentRequest GetPaymentRequest()
        {
            return new PaymentRequest()
            {
                CardHolder = "Mox Shah",
                CreditCardNumber = "4375510789011233",
                SecurityCode = 111,
                ExpirationDate = new DateTime(2019, 11, 01),
            }; ;
        }
    }
}
