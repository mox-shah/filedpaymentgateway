﻿import { Component, Inject, Input } from '@angular/core';
import { Http } from '@angular/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'dopayment',
    templateUrl: './dopayment.component.html'
})
export class DoPaymentComponent {
    paymentRequest: any = {
        CreditCardNumber: "4375-5107-8901-1233",
        CardHolder: "Mox Shah",
        ExpirationDate: "11/01/2021",
        SecurityCode: "111",
        Amount: "5000"
    };
    sampleData: any[] = [];
    http: Http;
    requestDetails: any = {
        requestUrl: "",
        requestStatus: "",
        requestStatusCode: "",
        response: ""
    }
    constructor(http: Http) {
        this.http = http;
        this.generateSampleData();
    }
    doPayment() {
        this.requestDetails = {
            requestUrl: "",
            requestStatus: "",
            requestStatusCode: "",
            response: ""
        };
        this.requestDetails.requestUrl = 'api/payment/ProcessPayment?CreditCardNumber=' + this.paymentRequest.CreditCardNumber + '&CardHolder=' + this.paymentRequest.CardHolder + '&ExpirationDate=' + this.paymentRequest.ExpirationDate + '&SecurityCode=' + this.paymentRequest.SecurityCode + '&Amount=' + this.paymentRequest.Amount + '';
        this.requestDetails.requestStatus = "Requesting";
        this.http.get(this.requestDetails.requestUrl).subscribe(result => {
            var response: any = result;
            this.requestDetails.requestStatus = "Completed";
            this.requestDetails.requestStatusCode = response.status;
            var obj = JSON.parse(response._body);
            if (obj.paymentType == PaymentMode.Cheap) {
                obj.paymentType = "Cheap";
            }
            if (obj.paymentType == PaymentMode.Expensive) {
                obj.paymentType = "Expensive";
            }
            if (obj.paymentType == PaymentMode.Premium) {
                obj.paymentType = "Premium";
            }
            var str = JSON.stringify(obj, undefined, 4);
            this.requestDetails.response = str;
            console.log(response._body)
        }, error => {
            this.requestDetails.requestStatus = "Completed";
            this.requestDetails.requestStatusCode = error.status;
            this.requestDetails.response = error._body;
            console.error(error);
        });
    }
    generateSampleData() {
        this.sampleData.push({
            Request: "Simple 200OK",
            Expected: "200 OK",
            IsError: false,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "5000"
            }
        });
        this.sampleData.push({
            Request: "Cheap Payment",
            Expected: "200 OK",
            IsError: false,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "20"
            }
        });
        this.sampleData.push({
            Request: "Expensive Payment",
            Expected: "200 OK",
            IsError: false,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "200"
            }
        });
        this.sampleData.push({
            Request: "Premium Payment",
            Expected: "200 OK",
            IsError: false,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "1000"
            }
        });
        this.sampleData.push({
            Request: "Incorrect credit card format",
            Expected: "400 Bad Request",
            IsError: true,
            Payload: {
                CreditCardNumber: "1111-1111-1111-1111",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "1000"
            }
        });
        this.sampleData.push({
            Request: "Expired Credit Card",
            Expected: "400 Bad Request",
            IsError: true,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2017",
                SecurityCode: "111",
                Amount: "1000"
            }
        });
        this.sampleData.push({
            Request: "3 Retry Logic",
            Expected: "500 Internal Server Error",
            IsError: true,
            Payload: {
                CreditCardNumber: "4375-5107-8901-1233",
                CardHolder: "Mox Shah",
                ExpirationDate: "11/01/2019",
                SecurityCode: "111",
                Amount: "999"
            }
        });
    }
    tryRequest(payload: any) {
        this.paymentRequest = payload;
    }
}
enum PaymentMode {
    Cheap = 1,
    Expensive = 2,
    Premium = 3
}
//export class PaymentRequest {
//    CreditCardNumber: string;
//    CardHolder: string;
//    ExpirationDate: string;
//    SecurityCode: number;
//    Amount: number;
//}