﻿namespace Filed.PaymentIntegration.Web.Controllers
{
    using System;
    using Filed.PaymentIntegration.BusinessInterface;
    using Filed.PaymentIntegration.BusinessModel;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Payment Controller
    /// </summary>
    /// <seealso cref="Controller" />
    /// <seealso cref="Controller" />
    [Produces("application/json")]
    [Route("api/Payment")]
    public class PaymentController : Controller
    {
        /// <summary>
        /// The service provider
        /// </summary>
        IServiceProvider serviceProvider;

        /// <summary>
        /// The payment processor
        /// </summary>
        IPaymentProcessor paymentProcessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentController" /> class.
        /// </summary>
        /// <param name="serviceProvider">The service provider.</param>
        /// <param name="paymentProcessor">The payment processor.</param>
        public PaymentController(IServiceProvider serviceProvider, IPaymentProcessor paymentProcessor)
        {
            this.serviceProvider = serviceProvider;
            this.paymentProcessor = paymentProcessor;
        }

        /// <summary>
        /// Processes the payment.
        /// </summary>
        /// <param name="paymentDetails">The payment details.</param>
        /// <returns></returns>
        [HttpGet("[action]")]
        public PaymentResponse ProcessPayment(PaymentRequest paymentDetails)
        {
            return paymentProcessor.ProcessPayment(paymentDetails);
        }
    }

}